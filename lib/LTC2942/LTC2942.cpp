//
//  LTC2942-1  Coulomb counter library
//  Author : Frank Vermeij
//  Date 11-12-2017
//


#include "Arduino.h"
#include <Wire.h>
#include "LTC2942.h"



LTC2942::LTC2942()
{
	PrevTime = millis(); // initialize Prevtime variable
	PrevCount = 0;
}

void LTC2942::SetContr2reg(byte Contr)
{
  C2 = Contr;
  writeReg8 (regAddressControl,C2);
}

float LTC2942::readVolt(void)
{
  // Read two bytes from regAddressVoltage and convert to Voltage
  int LTC2942VoltageRaw = readReg16(regAddressVoltage);
  float LTC2942Voltage = 6 * LTC2942VoltageRaw / 65535.000;
  return LTC2942Voltage; 
}

uint16_t LTC2942::readCount(void)
{

uint16_t Count;

  Count = readReg16(regAddressAccuCharge);
  return Count;
}

uint16_t LTC2942::writeCount(uint16_t V)
{
  writeReg16(regAddressAccuCharge,V);
  return V;
}

uint16_t LTC2942::computeCount(void)
{
float MyFl;
uint16_t Myuint;
//  Serial.println("ComputeCount");
  MyFl = readVolt();// readLipo battery voltage. BattmaxV = 4.2; BattMinV = 3.4; 
  //Serial.println(MyFl);
//  MyFl= MyFl - 3.4; // BattmaxV - BattMinV = 0.8;  65535 / 0.8 = 81910
  MyFl= MyFl - 3.4; // BattmaxV - BattMinV = 0.8;  47058 / 0.8 = 58822
  if (MyFl < 0) MyFl = 0;
 // Serial.println(MyFl);

//  MyFl = MyFl * 81910;
  MyFl = MyFl * 58822;
  Serial.println(MyFl);
//  if (MyFl > 65535) MyFl = 65535;
  if (MyFl > 47058) MyFl = 47058; // 
  Myuint = uint16_t(MyFl + 18478); // tel offset er bij
 // Serial.println(Myuint);
  writeCount(Myuint);
  return uint16_t(Myuint);
}



// This function tests if register PwrUp has a default value, meaning the system is 
// started by means of power up or (warm)reset
boolean LTC2942::CheckPowerUp(void)
{
uint8_t MyByte = 0x5A;
  MyByte = readReg8(PwrUp);
  if(MyByte == 'F')
  {
  	return false;
  }	
  else
  {
	writeReg8(PwrUp,'F');
	writeReg8(RstCnt,0); // reset RstCnt register
  	return true;
  }
}

uint8_t LTC2942::readMem(uint8_t Adr)
{
uint8_t MyByte = 0;
	MyByte = readReg8(Adr);
	return MyByte;	
}

void LTC2942::writeMem(uint8_t A, uint8_t V)
{
  writeReg8(A,V);
}

void LTC2942::IncrMem(uint8_t A)
{
uint8_t Mem;
	Mem = readMem(A);
	Mem++;
	writeMem(A,Mem);
}


float LTC2942::GetQ(void)
{
int MyCount;
int MyI;
byte MyC2 = C2;
float MyQ;
  MyC2 = MyC2 & 0x38; // mask = 00111000
  MyC2 = MyC2 >> 3; // Shift right 3 places
  MyQ = 1<<MyC2; 
  MyQ/= 128;
  MyQ*= 0.085;
  MyQ*= readCount();
//  Serial.println(MyF);
//  Serial.println();
  return MyQ;
}


float LTC2942::GetPerc(void)
{
// Berekening van ladingpercentage van accu.
// Uitgaande van een accu met een capaciteit van 2000mAh
// De prescaler van de LTC2942 wordt op 64 gezet Zie manual van deze chip.
// Met deze prescaler waarde en accu capaciteit is het bereik van de counter van de chip 47058
// Een waarde van 65536 is 100%. Een waarde van 65536 - 47058 = 18478 = 0%
// Berekening percentage:
// ((readCount() - 18478) / (65536 - 18478) * 100)
float MyPerc;
int MyCount;
float MyFlCount;
float offset = 18478.0;
float MaxCount = 65536.0;
  MyCount = readCount();
  MyFlCount = float(MyCount) - float(offset);
  MyFlCount = (float(MyCount - offset) / float(MaxCount - offset)) * 100;
 // Serial.println(MyFlCount);
  return MyFlCount;
}
 
  
float LTC2942::readTemp(void)
{
  // Read two bytes from regAddressTemp and convert to Celsius
  unsigned int LTC2943TempRaw = readReg16(regAddressTemp);
  float LTC2943Temp = (600.00 * LTC2943TempRaw / 65535.00) - 273.15;
  return LTC2943Temp;
}

void LTC2942::writeReg8(int reg, byte data)
{
  Wire.beginTransmission( LTC2942Address);
  Wire.write( reg);
  Wire.write( data);
  Wire.endTransmission();
}

void LTC2942::writeReg16(int reg, unsigned int data)
{
  Wire.beginTransmission(LTC2942Address);
  Wire.write( reg);                       // first byte is register address inside ltc 
  Wire.write( highByte( data));
  Wire.write( lowByte( data));
  Wire.endTransmission();
}


byte LTC2942::readReg8(int reg)
{
  byte data = 0;       // preset to zero, return zero if something wrong

  Wire.beginTransmission(LTC2942Address);
  Wire.write( reg);               // set register address inside ltc
  Wire.endTransmission(false);

  Wire.requestFrom(LTC2942Address, 1);
  data = Wire.read();
  

  return( data);
}


    
unsigned int LTC2942::readReg16( int reg)
{
  byte dataH, dataL;
  unsigned int data = 0;    // preset to zero, return zero if something wrong
//  Serial.print(reg);
//  Serial.print("( ");

  Wire.beginTransmission(LTC2942Address);
  Wire.write( reg);
  Wire.endTransmission(false);
  
  int n = Wire.requestFrom(LTC2942Address, 2);
  if( n == 2)
  {
    dataH = Wire.read();
    dataL = Wire.read();
//    Serial.print(dataL);
//    Serial.print(" ");
//    Serial.print(dataH);
//    Serial.print(" )");
    data = word( dataH, dataL);
  }
  else
  {
    Serial.println("ERROR, LTC2942 did not want to send 2 bytes");
  }

  return( data);
}


