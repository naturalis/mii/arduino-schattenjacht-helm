
// LTC2942 battery gauge library

#ifndef LTC2942_h
#define LTC2942_h

#include "Arduino.h"



// Define register addresses LTC2492-1 coulomb counter
#define LTC2942Address 0x64

#define regAddressStatus 0x00
#define regAddressControl 0x01
#define regAddressAccuCharge 0x02
#define Mem1 0x04
#define Mem2 0x05
#define Mem3 0x06
#define Mem4 0x07
#define regAddressVoltage 0x08
#define regAddressVoltageL 0x09
#define Mem5 0x0A
#define Mem6 0x0B
#define regAddressTemp 0x0C
#define regAddressTempL 0x0C
#define RstCnt 0x0E
#define PwrUp 0x0F

#define senseResistor 0.050

// Mem1 .. MemPwrUp are treshold registers  
// In case the treshold registers are not used for tresholding the can be used as volatile memory.

class LTC2942
{
  public:
    LTC2942(void);
	void SetContr2reg(byte Contr);
    float readVolt(void);
    uint16_t readCount(void);
    uint16_t writeCount(uint16_t V);
    uint16_t computeCount(void);
	float readTemp(void);
	boolean CheckPowerUp(void);
	float GetQ();
	float GetPerc(void);
    uint8_t readMem(uint8_t Adr);
    void writeMem(uint8_t A, uint8_t V);
	void IncrMem(uint8_t A);
	
  private:
    long int PrevTime;  // Used for calulating average current flow
	uint16_t PrevCount; //   ""
//	float AverageCount;	//   ""
	void writeReg8( int reg, byte data);
    void writeReg16( int reg, unsigned int data);
    byte readReg8( int reg);  
    unsigned int readReg16( int reg);
	byte C2; // contains value of Control2 register
	
};


#endif
