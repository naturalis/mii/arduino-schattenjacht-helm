#include "MMA8452_Simple.h"

// MMA8453_Simple* MMA8453_Simple::pMMA8453_Simple = 0; 


MMA8453_Simple::MMA8453_Simple()
{
//	pMMA8453_Simple = this;
	I2CAddr = 0x1D; //The i2C address of the MMA8453 chip. 0x1D is another common value.
}


void MMA8453_Simple::setI2CAddr(int address)
{
	I2CAddr = address; //I2CAddr; 
	
}

void MMA8453_Simple::writeRegister(MMA8452Q_Register reg, byte data)
{
  writeRegisters(reg, &data, 1);
}

void MMA8453_Simple::writeRegisters(MMA8452Q_Register reg, byte *buffer, byte len)
{
  Wire.beginTransmission(I2CAddr);
  Wire.write(reg);
  for (int x = 0; x < len; x++)
    Wire.write(buffer[x]);
  Wire.endTransmission(); //Stop transmitting
}


int MMA8453_Simple::readRegister(MMA8452Q_Register reg)
{
  Wire.beginTransmission(I2CAddr);
  Wire.write(reg);
  Wire.endTransmission(false); //endTransmission but keep the connection active
  Wire.requestFrom(I2CAddr, (byte) 1); //Ask for 1 byte, once done, bus is released by default
//  return Wire.peek();
  while(!Wire.available()) ; //Wait for the data to come back
  return Wire.read(); //Return this one byte
}

void MMA8453_Simple::readRegisters(MMA8452Q_Register reg, byte *buffer, byte len)
{
  Wire.beginTransmission(I2CAddr);
  Wire.write(reg);
  Wire.endTransmission(false); //endTransmission but keep the connection active

  Wire.requestFrom(I2CAddr, len); //Ask for bytes, once done, bus is released by default

  while(Wire.available() < len); //Hang out until we get the # of bytes we expect

  for(int x = 0 ; x < len ; x++)
    buffer[x] = Wire.read();    
}


void MMA8453_Simple::InitMotion(byte threshold, boolean enableINT2)
{
byte MyByte;
byte intSelect = 0x20;
	if(enableINT2) intSelect = 0x00;
	if(threshold > 127) threshold = 127; //a range of 0-127.

	MyByte = readRegister(CTRL_REG1);
	MyByte |= 0xFE;
	writeRegister(CTRL_REG1,MyByte);// Inactive Mode	
	
	
	writeRegister(TRANSIENT_CFG,0x1E);    // Transient Config
	writeRegister(TRANSIENT_THS,threshold);    // Transient Treshhold 
	writeRegister(TRANSIENT_COUNT,0x01);  // Transient Debounce; hier kan je de gevoeligheid mee instellen
	
//	writeRegister(CTRL_REG4,0x20);        // Transcient interrupt enable
	MyByte = readRegister(CTRL_REG4);
	MyByte |= 0x20;  // Transcient interrupt enable
	writeRegister(CTRL_REG4,MyByte);//
	
	MyByte = readRegister(CTRL_REG5);
	
	MyByte |= intSelect; // select int pin
	writeRegister(CTRL_REG5,MyByte);//
 

	MyByte = readRegister(CTRL_REG1);
	MyByte |= 0x01;
	writeRegister(CTRL_REG1,MyByte);//Write in the updated value to put the device in Active Mode	
}

void MMA8453_Simple::InitSleep(byte Timeout, boolean enableINT2)
{
byte MyByte;
// byte intSelect = 0x80;
//	if(enableINT2) intSelect = 0x00;
	
	MyByte = readRegister(CTRL_REG1);
	MyByte|= 0xFE;
	writeRegister(CTRL_REG1,MyByte); // Inactive Mode	
//	writeRegister(CTRL_REG1,0);// Inactive Mode
	
	writeRegister(ASLP_COUNT,Timeout);// Geef timeout een waarde
	
	MyByte = readRegister(CTRL_REG4);
	MyByte|= 0x80; //Auto-sleep/wake interrupt enabled. 
	writeRegister(CTRL_REG4,MyByte);//


	
	MyByte = readRegister(CTRL_REG5);
//	MyByte &= 0x7F; // clear bit 7
//	MyByte |= intSelect; //Interrupt is routed to INT2 pin
	MyByte&= 0x7F; //Interrupt is routed to INT2 pin
	writeRegister(CTRL_REG5,MyByte);//

	
	writeRegister(CTRL_REG3,0x40);//Transient function interrupt can wake up system 

	MyByte = readRegister(CTRL_REG2);
	MyByte |= 0x04; //Auto-sleep is enabled.
	writeRegister(CTRL_REG2,MyByte);//	
	
		MyByte = readRegister(CTRL_REG1);
	MyByte |= 0x01;
	writeRegister(CTRL_REG1,MyByte);//Write in the updated value to put the device in Active Mode	
}	

void MMA8453_Simple::InitOriDet(byte Tresh, boolean enableINT2)
{
byte MyByte;
byte intSelect = 0x10;
	if(enableINT2) intSelect = 0x00;
	if(Tresh > 127) Tresh = 127; //a range of 0-127.
	
// Step 1: Put the part into Standby Mode
	MyByte = readRegister(CTRL_REG1);
	MyByte |= 0xFE;
	writeRegister(CTRL_REG1,MyByte);// Inactive Mode	
	Serial.print("MMA8452 Cntr1: ");
	Serial.println(MyByte);
	
// Step 2: Set the data rate to 50 Hz (for example, but can choose any sample rate).
	MyByte = readRegister(CTRL_REG1); //Note: Can combine this step with above
	MyByte&= 0xC7; //Clear the sample rate bits
	MyByte|= 0x20; //Set the sample rate bits to 50 Hz
	writeRegister(CTRL_REG1,MyByte);//Write updated value into the register.
	
// Step 3: Set the PL_EN bit in Register 0x11 PL_CFG. This will enable the orientation detection.
	MyByte = readRegister(PL_CFG);
	MyByte |= 0x40;
	writeRegister(PL_CFG, MyByte);
	
// Step 8: Register 0x2D, Control Register 4 configures all embedded features for interrupt detection.
	MyByte = readRegister(CTRL_REG4); //Read out the contents of the register
	MyByte |= 0x10; //Set bit 4
	writeRegister(CTRL_REG4, MyByte); //Set the bit and write into CTRL_REG4

//Step 9: Register 0x2E is Control Register 5 which gives the option of routing the interrupt to either INT1 or INT2	
	MyByte = readRegister(CTRL_REG5); //Read out the contents of the register
	MyByte &= 0xEF; //Clear bit 4 to choose the interrupt to route to INT2
	writeRegister(CTRL_REG5, MyByte); //Set the bit and write into CTRL_REG4
	
//Step 10: Set the debounce counter in register PL_COUNT (0x12)
	writeRegister(PL_COUNT, 0x02); //This sets the debounce counter to 100 ms at 50 Hz	

// Step 11: Put the device in Active Mode
	MyByte = readRegister(CTRL_REG1);
	MyByte |= 0x01;
	writeRegister(CTRL_REG1,MyByte);//Write in the updated value to put the device in Active Mode	
}




// Single Tap Only: Normal Mode, No LPF, 400 Hz ODR. Zie document AN4072.pdf pag 17
void MMA8453_Simple::Init_S_Tap(byte TrX,byte TrY,byte TrZ)
{
byte MyByte;
//byte intSelect = 0x80;
//	if(enableINT2) intSelect = 0x00;

// Step 1: To set up any configuration make sure to be in Standby Mode	
	MyByte = readRegister(CTRL_REG1);
	MyByte |= 0xFE;
	//writeRegister(CTRL_REG1,MyByte);// Inactive Mode	
	writeRegister(CTRL_REG1,0);// Inactive Mode

//Step 2: Enable X and Y and Z Single Pulse
writeRegister(PULSE_CFG,0x15);	

// Step 3: Set Threshold 1.575g on X and 2.65g on Z
	writeRegister(PULSE_THSX,TrX);
	writeRegister(PULSE_THSY,TrY);
	writeRegister(PULSE_THSZ,TrZ);

// Step 4: Set Time Limit for Tap Detection to 50 ms, Normal Mode, No LPF
// Data Rate 400 Hz, time step is 0.625 ms
	writeRegister(PULSE_TMLT,0x50);
	
// Step 5: Set Latency Time to 300 ms
	writeRegister(PULSE_LTCY,0xF0);
	
// Step 6: Route INT1 to System Interrupt
writeRegister(CTRL_REG4,0x08);
writeRegister(CTRL_REG5,0x08);

// Step 7: P u t t h e d e v i c e i n Active Mode
MyByte = readRegister(CTRL_REG1);
MyByte |= 0x01;
writeRegister(CTRL_REG1,MyByte);//Write in the updated value to put the device in Active Mode	
}


// Double Tap Only: Normal Mode, No LPF, 400 Hz ODR. Zie document AN4072.pdf pag 15
void MMA8453_Simple::Init_D_Tap(byte TrX,byte TrY,byte TrZ)
{
byte MyByte;
//byte intSelect = 0x80;
//	if(enableINT2) intSelect = 0x00;

// Step 1: To set up any configuration make sure to be in Standby Mode	
	MyByte = readRegister(CTRL_REG1);
	MyByte |= 0xFE;
	//writeRegister(CTRL_REG1,MyByte);// Inactive Mode	
	writeRegister(CTRL_REG1,0);// Inactive Mode

//Step 2: Enable X, Y and Z Double Pulse with DPA = 0 no double pulse abort
writeRegister(PULSE_CFG,0x2A);	

// Step 3: Set Threshold  X,Y,Z
	writeRegister(PULSE_THSX,TrX);
	writeRegister(PULSE_THSY,TrY);
	writeRegister(PULSE_THSZ,TrZ);

// Step 4: Set Time Limit for Tap Detection to 60 ms LP Mode
// Note: 400 Hz ODR, Time step is 1.25 ms per step
	writeRegister(PULSE_TMLT,0x30);
	
// Step 5: Set Latency Time to 200 ms
	writeRegister(PULSE_LTCY,0x50);
	
// Step 6: Set Time Window for second tap to 300 ms	
	writeRegister(PULSE_WIND,0x78);
// Step 7: Route INT1 to System Interrupt
	writeRegister(CTRL_REG4,0x08);
	writeRegister(CTRL_REG5,0x08);

// Step 8: P u t t h e d e v i c e i n Active Mode
	MyByte = readRegister(CTRL_REG1);
	MyByte |= 0x01;
	writeRegister(CTRL_REG1,MyByte);//Write in the updated value to put the device in Active Mode
}

// Single Tap and Double Tap LP Mode, 200 Hz ODR. Zie document AN4072.pdf pag 16
void MMA8453_Simple::Init_SD_Tap(byte TrX,byte TrY,byte TrZ)
{
byte MyByte;
//byte intSelect = 0x80;
//	if(enableINT2) intSelect = 0x00;

// Step 1: To set up any configuration make sure to be in Standby Mode	
	MyByte = readRegister(CTRL_REG1);
	MyByte |= 0xFE;
	//writeRegister(CTRL_REG1,MyByte);// Inactive Mode	
	writeRegister(CTRL_REG1,0);// Inactive Mode

//Step 2: Enable X, Y and Z Double Pulse with DPA = 0 no double pulse abort
writeRegister(PULSE_CFG,0x7F);	// Let op ELE bit is gezet. Programma moet PULSE_SRC lezen om interrupt lijn weer vrij te geven

// Step 3: Set Threshold  X,Y,Z
	writeRegister(PULSE_THSX,TrX);
	writeRegister(PULSE_THSY,TrY);
	writeRegister(PULSE_THSZ,TrZ);

// Step 4: Set Time Limit for Tap Detection to 60 ms (LP Mode, 200 Hz ODR, No LPF)
// Note: 200 Hz ODR LP Mode, Time step is 2.5 ms per step
	writeRegister(PULSE_TMLT,0x18);
	
// Step 5: Set Latency Time to 200 ms
// Note: 200 Hz ODR LP Mode, Time step is 5 ms per step
	writeRegister(PULSE_LTCY,0x28);
	
// Step 6: Set Time Window for second tap to 300 ms	
// Note: 200 Hz ODR LP Mode, Time step is 5 ms per step
	writeRegister(PULSE_WIND,0x8C);
	
// Step 7: Route INT1 to System Interrupt
	writeRegister(CTRL_REG4,0x08);
	writeRegister(CTRL_REG5,0x08);

// Step 8: P u t t h e d e v i c e i n Active Mode
	MyByte = readRegister(CTRL_REG1);
	MyByte |= 0x01;
	writeRegister(CTRL_REG1,MyByte);//Write in the updated value to put the device in Active Mode
}

