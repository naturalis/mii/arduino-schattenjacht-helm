
#ifndef MMA8452_Simple_H
#define MMA8452_Simple_H

#include "Arduino.h"    
#include <Wire.h>



enum MMA8452Q_Register {
  MMA8452STATUS = 0x00,
  OUT_X_MSB = 0x01,
  OUT_X_LSB = 0x02,
  OUT_Y_MSB = 0x03,
  OUT_Y_LSB = 0x04,
  OUT_Z_MSB = 0x05,
  OUT_Z_LSB = 0x06,
  SYSMOD = 0x0B,
  INT_SOURCE = 0x0C,
  WHO_AM_I = 0x0D,
  XYZ_DATA_CFG = 0x0E,
  HP_FILTER_CUTOFF = 0x0F,
  PL_STATUS = 0x10,
  PL_CFG = 0x11,
  PL_COUNT = 0x12,
  PL_BF_ZCOMP = 0x13,
  P_L_THS_REG = 0x14,
  FF_MT_CFG = 0x15,
  FF_MT_SRC = 0x16,
  FF_MT_THS = 0x17,
  FF_MT_COUNT = 0x18,
  TRANSIENT_CFG = 0x1D,
  TRANSIENT_SRC = 0x1E,
  TRANSIENT_THS = 0x1F,
  TRANSIENT_COUNT = 0x20,
  PULSE_CFG = 0x21,
  PULSE_SRC = 0x22,
  PULSE_THSX = 0x23,
  PULSE_THSY = 0x24,
  PULSE_THSZ = 0x25,
  PULSE_TMLT = 0x26,
  PULSE_LTCY = 0x27,
  PULSE_WIND = 0x28,
  ASLP_COUNT = 0x29,
  CTRL_REG1 = 0x2A,
  CTRL_REG2 = 0x2B,
  CTRL_REG3 = 0x2C,
  CTRL_REG4 = 0x2D,
  CTRL_REG5 = 0x2E,
  OFF_X = 0x2F,
  OFF_Y = 0x30,
  OFF_Z = 0x31
};



class MMA8453_Simple {

public:
//  friend void accelISR(void); //make friend so bttnPressISR can access private var keyhit	
 
  MMA8453_Simple();

/***********************************************************
 * I2C functions
 */
 
void setI2CAddr(int address);
void writeRegister(MMA8452Q_Register reg, byte data);
void writeRegisters(MMA8452Q_Register reg, byte *buffer, byte len);
int readRegister(MMA8452Q_Register reg);
void readRegisters(MMA8452Q_Register reg, byte *buffer, byte len);
void InitMotion(byte threshold, boolean enableINT2);
void InitSleep(byte Timeout, boolean enableINT2);
void InitOriDet(byte Tresh, boolean enableINT2);
void Init_S_Tap(byte TrX,byte TrY,byte TrZ);
void Init_D_Tap(byte TrX,byte TrY,byte TrZ);
void Init_SD_Tap(byte TrX,byte TrY,byte TrZ);

private:
	byte I2CAddr;
 };
 
#endif
	
