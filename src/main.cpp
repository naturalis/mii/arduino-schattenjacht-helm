#include <Arduino.h>

/*
Scetch: Helm ESP32


  
Hardware:
Arduino systeem met ESP WROOM32


*/

//==========================================================================
// Includes
//==========================================================================

#include <Wire.h>
#include <Adafruit_GFX.h> // Core graphics library
#include <Adafruit_SSD1306.h>
//#include <ArduinoJson.h>
#include "DFRobotDFPlayerMini.h" // MP3 player
#include "LTC2942.h"             // Battery Gauge - Coulomb meter
#include "MMA8452_Simple.h"      // Accelerometer library.
#include "SWTimer.h"
#include "esp_system.h"
#include <HardwareSerial.h> // ESP32 serial library
#include "SimpleBLE.h"
#include "esp_deep_sleep.h"
//#include "esp_sleep.h"
//==========================================================================
//Defines
//==========================================================================

//#define noInterrupts() {portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;portENTER_CRITICAL(&mux)
//#define interrupts() portEXIT_CRITICAL(&mux);}

#define Version "1.20"


// Software timer configuratie
#define NrTimers 10
#define SystemProcesTimer 1
#define OledMesgTimer 2
#define SendStatusTimer 3
#define CheckExtPowerTimer 4
#define BLETimeoutTimer 5
#define BattLowLedTimer 6
#define IRInhibTimer 7
#define CheckOrientatieTimer 8
#define AudioDelayTimer 9


// IR signaal configuratie
// MaxIrCod bepaalt aantal mogelijke IR codes
#define MaxIrObj 14
#define MaxIrCod 22
#define MaxIRTrans 24
#define MaxIRDuration 250

//MMA8452 accelerometer
#define MMA8452Sensib 1

// ESP Wroom 32 I/O pin configuratie

// reset output voor Oled. wordt niet gebruikt.
#define OLED_RESET 4 // uitzoeken!

#define Motion_intr GPIO_NUM_13
#define LedSpot GPIO_NUM_14
#define MMA8452Int2 GPIO_NUM_15

// I2C bus
#define SCL 16
#define SDA 17

// Uart01 RX and TX are connected to MP3 player
#define Mp3Rx GPIO_NUM_21
#define Mp3Tx GPIO_NUM_22
//#define Enable_Mp3 23
#define Enable_Mp3 25
#define BattLowLed 5
#define IRSignal GPIO_NUM_27

#define V_ID GPIO_NUM_32
#define V_LedSpot GPIO_NUM_33
#define Sense_ExtV GPIO_NUM_34
#define V_AudioVol GPIO_NUM_35

#define RXD U0RXD
#define TXD U0TXD

//==========================================================================
// Typedef's
//==========================================================================

typedef union {

  uint64_t num64;
  uint32_t num32;
  uint16_t num16;
} tIntConv;

//==========================================================================
// Memory declarations
//==========================================================================

// ============== BLE (Bluetooth Low Energy)
SimpleBLE ble;

volatile int UniqID = 100; // Variabele wordt gebruikt om BLE advertising info van een unieke tag te voorzien
int wakeup_reason;  // Systeem variable. Heeft info over hoe de processor gestart is. Timer ,resetbtn of accelerometer.

// ========= Helmet ID;
int MyId = 0; //

// ========= ESP32 MAC 
uint8_t chipid[6];

// Serial comm:

String InStringCom = ""; // String voor seriele configuratie

// ========= Infrarood code decodering:
volatile tIntConv IRTrancient[MaxIRTrans]; // Array waar IR pulstijden worden opgeslagen
volatile int IRTr_p = 0;                   // index in IRTrancient array
boolean IRFlag = false;                    // Wordt true als er een IR code ontvangen is
unsigned long IRDuration;                  // variable om tijdsduur van IR code te meten;
int IRerc = 0;                             // Error variable voor IR code
unsigned int NrIR_ISR = 0;                 // Teller houdt bij hoeveel interrupts hebben plaatsgevonden tijdens ontvangst IRCode (Debugging)
int IRTimeOut = 0;
int IRKey = -2;
int PrevIRKey = -1;
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

// ========= State machines:
// IR signal states
enum _IRState
{
  sWAITING,
  sPENDING,
  sFINISHING
};
volatile enum _IRState IRState = sWAITING;

enum _OperationMode
{
  NoOpr,
  Charging,
  Playing
};
volatile enum _OperationMode OprMode = NoOpr;
volatile enum _OperationMode PrevOprMode = NoOpr;


// ========= Oled display;
Adafruit_SSD1306 display(OLED_RESET);

//========= Hardware timers
hw_timer_t *timer0 = NULL;
hw_timer_t *timer1 = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

// ========= Software timers
SWTimer SW_Timer[NrTimers]; // Array of timer variables (volatile?)
int SysTimDel = 100;     // Delay variable. Zie DoScheduler()
int SendStatDel = 90000; // Delay variable. Zie DoScheduler()
enum _StatItems
{
  sVBAT,
  sHELMTEMP,
  sHELMGauge,
  sHELMBatP,
  sIDLESTAT
}; // Status-item states; Zie DoScheduler()
volatile enum _StatItems StatItems = sVBAT;

//=================LedSpot ==============
byte LedSpotLevelMax = 0; // Maximale niveau waarop ledspot mag branden
byte LedSpotTarget = 0;   // Niveau waarop ledspot moet gaan branden.
byte LedSpotLevel = 0;    // variable huidige niveau.
int ledChannel_0 = 0;
int freq = 5000;
int resolution = 8;

//=================MP3 player ==============
DFRobotDFPlayerMini myDFPlayer;
int AudioVolume = 18;
HardwareSerial Mp3Serial(1);
int MyTrack; 

//=================Batt Low Led ==============

boolean BLL = false; // Boolean die bepaalt of Batt low led aan of uit is

// ==== LTC2942-1 coulomb counter =========
// De LTC2942-1 chip meet de hoeveelheid energie die de batterij in en uitgaat
LTC2942 BattGauge;
uint16_t BattCouombCount; //
uint16_t PrevBattCouombCount = 0;

byte LTC2942StatusRaw;
uint16_t CoulombCount;
uint16_t PrevCoulombCount;
int CoulombCountDiv;
float AccuVoltage;
float AccuCharge;
unsigned int LTC2942TempRaw;
float LTC2942Temp;

// ====== Accelerometer ==========
// De MM8452 accelerometer chip meet of de helm in beweging is. Als de helm 81 seconden niet beweegt
// geeft de chip een "Go to sleep" interrupt en schakelt het systeem over op Sleep mode.
// Als de helm weer bewogen wordt wordt een wake up interrupt gegeven
// De chip maakt gebruik van de I2C bus en heeft op deze bus adres 0x1D.

MMA8453_Simple MMA8452;
boolean motion = false;
boolean MMA8452Int2Fl = false;
byte HelmOriCnt = 0; // Counter telt hoe lang de helm aan de ophanghaak hangt. 

// Functie declaraties van interrupt routines. Moeten bovenaan in de code staan bij ESP32
void IRAM_ATTR onTimer0();
void IRAM_ATTR IR_ISR();
void IRAM_ATTR Motion_ISR(void);
void IRAM_ATTR MMA8452_Int2_ISR(void);
void IRAM_ATTR IR_ISR();

// =================  End of memory declarations  ===================================================

// =================================================================================================
// OLed display functions
// =================================================================================================

void DrawText(int X, int Y, boolean Clear, const char *Text)
{
  if (Clear)
    display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(X, Y);
  display.println(Text);
  display.display();
}

void ShowMesg(const char *Text)
{
  display.fillRect(0, 17, 127, 32, BLACK); // Wis onderste helft van display
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(1, 25);
  display.println(Text);
  display.display();
  SW_Timer[OledMesgTimer].Set(50); // 5 seconden
}

void clearOledDisplay()
{    
  display.clearDisplay();
  display.display();
}

void ShowMesgP(const char *Text, int Prm)
{
  display.fillRect(0, 17, 127, 32, BLACK); // Wis onderste helft van display
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(1, 25);
  display.print(Text);
  display.print(" ");
  display.println(Prm);
  display.display();
  SW_Timer[OledMesgTimer].Set(50); // 5 seconden
}

void ShowHelmID(void)
{
  String MyStr;
  char MyChr[32];

  display.fillRect(0, 1, 127, 16, BLACK); // Wis onderste helft van display
  display.setTextSize(2);
  display.setTextColor(WHITE);

  MyStr = "Helm";
  MyStr.concat(int(MyId));
  MyStr.toCharArray(MyChr, 32);
  display.setCursor(1, 1);
  display.println(MyChr);
  display.display();
}

void  ShowEnergyStatus(void)
{
  String MyStr;
  char MyChr[32];

  display.fillRect(0, 44, 127, 63, BLACK); // Wis onderste helft van display
  display.setTextSize(2);
  display.setTextColor(WHITE);

  MyStr = "Batt: ";
  //  MyStr.concat(BattGauge.readVolt());
  //  MyStr.concat(" (");
  MyStr.concat(int(BattGauge.GetPerc()));
  MyStr.concat("%");
  MyStr.toCharArray(MyChr, 32);
  display.setCursor(1, 50);
  display.println(MyChr);
  display.display();
}


void LowPowerMsg(void)
{
  String MyStr;
  char MyChr[32];

  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  MyStr = "Low Power!";
  MyStr.toCharArray(MyChr, 32);
  display.setCursor(1, 10);
  display.println(MyChr);
  display.setTextSize(1);
  MyStr = BattGauge.readVolt();
  MyStr.concat(" volt");
  MyStr.toCharArray(MyChr, 32);
  display.setCursor(1, 40);
  display.println(MyChr);
  display.display();
}

//== End of OLed display functions =================================================================

// =================================================================================================
//== System utilities
// =================================================================================================

boolean IsExternalVoltage(void) // Controleer of externe (laad) spanning aanwezig is
{
  int Vint = analogRead(Sense_ExtV);
  //   Serial.println(Vint);
  if (Vint > 1500)
    return true;
  else
    return false;
}


int GetHelmID(void)
{
  int MyInt;
  MyInt = (analogRead(V_ID) / 256) + 1;
  return MyInt;
}

//== MP3 player ==============================================
void PlaySound(int n)
{
  myDFPlayer.play(n);
}

int GetAudioVolume(void)
{
int MyInt = 0;  
  MyInt = analogRead(V_AudioVol) / 127;
  if (MyInt > 30)
    MyInt = 30;
  return MyInt;  
}
//== Ledspot =================================================

int GetLedSpotLevel(void)
{
  return analogRead(V_LedSpot) / 16;
}


void SetLedSpotLevel(byte MyByte)
{
  ledcWrite(ledChannel_0, MyByte);
}

void GoToSleep(void)
{
  //Timer0 uit
  timerAlarmDisable(timer0);
  // mp3 uit
  Serial.println("Sleeping");
  digitalWrite(Enable_Mp3, LOW); // Switch off (power consuming) MP3Player
 
  // LedSpot uit
  SetLedSpotLevel(0);
//  DrawText(0, 30, true, "");
  clearOledDisplay();
  delay(500);
  esp_deep_sleep_start();
}


void HandleMMA8452Int2(void) // Accelerometer intr2 afhandeling
{
byte MyByte = 0;
    MMA8452Int2Fl = false;
    Serial.println("MMA8452Int2");
    MyByte = MMA8452.readRegister(INT_SOURCE); // Check wat de reden van de interrupt is

    if(MyByte & 0b10000000) // SRC_ASLP interrupt. Dit is de slaapmodus interrupt
    {
      MyByte = MMA8452.readRegister(SYSMOD); // Door het lezen van bits in het register Sysmod kan de oorsprong van de interrupt bepaald worden.
      if ((MyByte & 0x01) == 0x01) // Bit 0: De accelerometer komt uit slaapmodus;
      {
        Serial.println("Wake up!");
      }
      if ((MyByte & 0x02) == 0x02) // Bit 1: De accelerometer gaat in slaapmodus. De helm volgt en gaat ook slapen
      {
        Serial.println("I'm going to sleep");
        GoToSleep(); // Helm gaat in slaapmodus.
      }
    }
/*
    if(MyByte & 0b00010000) // SRC_LNDPRT  (Landscape / portrait) interrupt.  Wordt in deze versie niet gebruikt
    {
      MyByte = MMA8452.readRegister(PL_STATUS);
      Serial.print("PL_STATUS :");
      Serial.println(PL_STATUS,BIN);
    }
*/
}


void CheckMotion()
{
  if (motion)
  {
    motion = false;
    MMA8452.readRegister(TRANSIENT_SRC);
    Serial.print("!");
  }
}

void CheckSleepModus()
{
  if (MMA8452Int2Fl) // bewegingssensor slaapmodus interrupt flag
  {
    HandleMMA8452Int2();
  }
}


void ResetMe(void) // Gecontroleerde system reset
{
  Serial.println("software reset");
  ESP.restart();
}

// =================================================================================================
//== LTC2942-1 coulomb counter / energy gauge
// =================================================================================================

void ReadEneryStatus(void)
{
  CoulombCount = BattGauge.readCount();
  Serial.println(CoulombCount);
  CoulombCountDiv = CoulombCount - PrevCoulombCount;
  PrevCoulombCount = CoulombCount;
  AccuVoltage = BattGauge.readVolt();
  LTC2942Temp = BattGauge.readTemp();
  AccuCharge = BattGauge.GetQ();
}

// ==== End of LTC2942-1 coulomb counter / enery gauge

// =================================================================================================
//== Accelerometer interrupt routines.
// =================================================================================================

void IRAM_ATTR Motion_ISR(void)
{
  portENTER_CRITICAL_ISR(&mux);
  motion = true;
  UniqID++; // Maak UniqID nog unieker door bewegingstoevalligheid
            //  digitalWrite(BattLowLed,HIGH);
  portEXIT_CRITICAL_ISR(&mux);
}

void IRAM_ATTR MMA8452Int2_ISR(void)
{
  portENTER_CRITICAL_ISR(&mux);
  MMA8452Int2Fl = true;
  portEXIT_CRITICAL_ISR(&mux);
}
// ==== End of Accelerometer interrupt routines.



// =================================================================================================
//== BLE functies  (Bluetooth Low Energy)
// =================================================================================================

String GetHelmName()
{
  String MyStr;
  MyId = GetHelmID();
  MyStr = "H_";
  if (MyId < 10)
    MyStr += "0";
  MyStr += String(MyId);
  return MyStr;
}

String AddUniqueID(void)
{
  String MyStr = "/";
  MyStr += String(++UniqID);
  if (UniqID > 9999)
    UniqID = 100;
  return MyStr;
}

String AdvertiseObjMsg(int Obj)
{
  String MyStr = GetHelmName();
  MyStr += String("_Obj:");
  MyStr += String(Obj);
  MyStr += AddUniqueID();
  ble.begin(MyStr);
  SW_Timer[BLETimeoutTimer].Set(30); // Timer loopt over 3 seconde af. Zie 'DoScheduler'
  return MyStr;
}

String AdvertiseVBattMsg()
{
  String
      MyStr = GetHelmName();
  MyStr += String("_VBat:");
  MyStr += BattGauge.readVolt();
  MyStr += AddUniqueID();
  ble.begin(MyStr);
  return MyStr;
}

String AdvertiseTempMsg()
{
  String
      MyStr = GetHelmName();
  MyStr += String("_Temp:");
  MyStr += BattGauge.readTemp();
  MyStr += AddUniqueID();
  ble.begin(MyStr);
  return MyStr;
}

String AdvertiseGaugeMsg()
{
  String
      MyStr = GetHelmName();
  MyStr += String("_Gauge:");
  MyStr += BattGauge.readCount();
  MyStr += AddUniqueID();
  ble.begin(MyStr);
  return MyStr;
}

String AdvertiseBatPMsg()
{
  String
      MyStr = GetHelmName();
  MyStr += String("_Bat%:");
  MyStr += BattGauge.GetPerc();
  MyStr += AddUniqueID();
  ble.begin(MyStr);
  return MyStr;
}

void SendObjBleMsg(int Msg)
{
  Serial.println(AdvertiseObjMsg(Msg)); // Gevonden object wordt in BLE Advertising Info getoond
}

// ==== End of BLE functions


//== Infrarood  =================================================


// De IR code is simpel opgebouwd. De pulsen zijn allemaal even lang maar de interval tussen deze pulsen zijn bepalend voor de waarde van de code.
// De minimale interval is 2000 microseconden. (Code 0). Elke opvolgende code is 500 microseconden langer. Dus 2500 us = code 1. 3000us is code 2 enz
// Ondestaande functie controleert of de lengte van de interval een valide waarde heeft. Als de waarde binnen 100us van een goede waarde valt klopt deze.
int MeetPuls(uint16_t P) //
{
  int i = 0;
  int Res = -1;
#define Spread 100

  while ((Res == -1) && (i < MaxIrCod))
  {
    if ((P > ((2000 + i * 500) - Spread) && (P < ((2000 + i * 500) + Spread)))) // De
      Res = i;
    i++;
  }
  return Res;
}



// Onderstaande functie klasificeert de gemeten intervaltijden en soorteert deze in een array.
// vervolgens wordt gekeken welke interval het meest voorkomt in het array. 
// De index van deze positie in het array is de gedecodeerde code.
int DecodeIRSignal()
{
  int i;

  int DecodArr[MaxIrCod + 1];
  int Pw;                        // Pulse gewicht factor
  int PId = 0;                   // Puls ID
  for (i = 0; i < MaxIrCod; i++) // clear DecodArr array
    DecodArr[i] = 0;

  for (i = 1; i < IRTr_p; i++) // Scan door gevonden pulstijdwaarden
  {
    PId = MeetPuls(IRTrancient[i].num16); //  PId krijgt een waarde als de gevonden puls aan de criteria van MeetPuls voldoet.
    if (PId != -1)
    {
      DecodArr[PId]++; //  Als PId een waarde heeft verhoog dan de waarde in DecodArr[PId];
    }
  }
  // Sorteer waarden in DecodArr
  Pw = 1;
  PId = -1;
  for (i = 0; i < MaxIrCod; i++) //Zoek in array welke positie de hoogste waarde heeft. i.e. gewicht. Hoe hoger hoe groter de zekerheid dat PId geldig is
  {
    if (DecodArr[i] > Pw)
    {
      Pw = DecodArr[i];
      PId = i;
    }
  }            // aan het einde van deze loop is pw gelijk aan het aantal van de meest voorkomende interval. PId bevat de gedecodeerde code.
  IRerc = 0;   // misschien uitbreiden met error codering
  if ((PId != 0) && (Pw > 2)) // Als PId een geldige waarde heeft en het bijhorende gewicht > 2 dan retourneert de functie PId
    return (PId);
  else
    return -1;
}

void ClearIRTr(void)
{
  int i;
  for (i = 0; i <= MaxIRTrans; i++)
    IRTrancient[i].num64 = 0;
}

void CheckIRCode()
{
  if (IRFlag)
  {
  IRFlag = false;
  IRKey = DecodeIRSignal();
  if ((IRKey != -1) && (IRKey <= MaxIrObj) && (SW_Timer[IRInhibTimer].Expired()))
  {
    SW_Timer[IRInhibTimer].Set(50); // Deze timer voorkomt dat er een nieuw item geslecteerd wordt terwijl het vorige nog uitgevoerd wordt;
    PrevIRKey = IRKey;
    {
      Serial.print("IR code ontvangen: ");
      Serial.println(IRKey);
      MyTrack = IRKey;
      SW_Timer[AudioDelayTimer].Set(8); // Audio track wordt gestart als timer afgelopen is.
      SendObjBleMsg(IRKey);
      ShowMesgP("Obj", IRKey);
      MMA8452.InitSleep(255, true);     // reset Sleep en wakeup functie in MM8452 accelerometer chip; Lange inSlaaptijd
    }
  }
  ClearIRTr();
  NrIR_ISR = 0;
  IRState = sWAITING;
  }
}
//==Einde van infrarood code ontvanger ============================================================================



// ==================================================================================================
//   S E T U P
// ==================================================================================================
void setup()
{
  int i;
  int StartTune = 15; // "Welkom bij de Schattenjacht"
 // byte MyByte = 0;
  String MyStr;

  //======= I/O configuratie=====================

  pinMode(LedSpot, OUTPUT);              // Met deze output wordt de BattLowLed aan/uit gezet
  digitalWrite(LedSpot, LOW);            // LOW, BattLowLed staat uit
  pinMode(IRSignal, INPUT_PULLUP);       // InterruptInput voor IR receiver.
  pinMode(Motion_intr, INPUT_PULLUP);    // InterruptInput voor MM8452 accerlerometer chip. (beweging)
  pinMode(MMA8452Int2, INPUT_PULLUP);    // InterruptInput voor MM8452 accerlerometer chip. (Sleep / Wake-up en orientatie)
  pinMode(Enable_Mp3, OUTPUT);           // Met deze output kan de MP3 module aan of uit gezet worden
  digitalWrite(Enable_Mp3, LOW);         // High, MP3 module staat uit
  pinMode(BattLowLed, OUTPUT);           // Met deze output wordt de BattLowLed aan/uit gezet
  digitalWrite(BattLowLed, LOW);         // LOW, BattLowLed staat uit
  pinMode(Sense_ExtV, INPUT);            // Met deze analoge input wordt gemeten of de accu opgeladen wordt

  // Analoge inputs ================
  pinMode(V_ID, INPUT);       // Het spannings niveau op deze analoge ingang bepaalt de ID van de helm (1..16)
  pinMode(V_LedSpot, INPUT);  // Het spannings niveau op deze analoge ingang bepaalt het lichtniveau van de led-spot
  pinMode(V_AudioVol, INPUT); // Het spannings niveau op deze analoge ingang bepaalt het geluidsniveau van de MP3 player
                              //  analogReadResolution(8);
                              //  analogSetAttenuation(ADC_6db);

  // Serieele communicatie  ===================

  Serial.begin(115200);
  Mp3Serial.begin(9600, SERIAL_8N1, Mp3Rx, Mp3Tx); // Serieele communicatie met MP3 player

  //======= Wakeup_Reason:  bepaal wat de processor heeft doen resetten =========
  wakeup_reason = esp_sleep_get_wakeup_cause(); // wakeup_reason heeft invloed op de werking van de helm.
  switch (wakeup_reason)
  {
  case ESP_SLEEP_WAKEUP_EXT0:
    MyStr = "EXT0 RTC_IO BTN";
    break;
  case ESP_SLEEP_WAKEUP_EXT1:
    MyStr = "EXT1 RTC_CNTL";
    break;
  case ESP_SLEEP_WAKEUP_TIMER:
    MyStr = "TIMER";
    break;
  case ESP_SLEEP_WAKEUP_TOUCHPAD:
    MyStr = "TOUCHPAD";
    break;
  case ESP_SLEEP_WAKEUP_ULP:
    MyStr = "ULP PROGRAM";
    break;
  default:
    MyStr = "NO DS CAUSE";
    break;
  }
  Serial.println("");
  Serial.print("Wakeup reason = ");
  Serial.println(MyStr);

  //====== I2C bus initialisatie  ===================
  Wire.begin(SDA, SCL);

  // OLed display  ============================
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();
  display.display();
  Serial.println("Init Oled");

  //====== LTC2942-1 Battery Gauge  ========================================

  //  BattGauge.SetContr2reg(0xEA);   // Set value of Control2 register
  BattGauge.SetContr2reg(0xF2); // Stel prescaler in met bits 5,4 en 3. 0xF2 = prescaler 2^6 = 64;

  if (BattGauge.CheckPowerUp()) // Check for Cold boot or Warm reset
  {
    DrawText(90, 5, false, "(C)"); // To do: geef gauge registers een geschatte waarde naar aanleiding van het batterijniveau
    BattGauge.computeCount();
  }
  else
    DrawText(90, 5, false, "(W)");
  Serial.println("Init LTC2942");

  // Low voltage Check ===============
  while (BattGauge.readVolt() < 3.4) // meet de accuspanning. Als deze kleiner is dan 3.4 volt dan niet starten
  {                                  // De ESPWroom32 werkt al bij 2.7 volt. De spanningeregelaar heeft echter een voltage drop van 0.9 volt
    LowPowerMsg();
    Serial.println(BattGauge.readVolt());
    esp_deep_sleep_enable_timer_wakeup(15000000);
    esp_deep_sleep_start(); // De Esp32 processor gaat 15 seconden in deep sleep modus en verbruikt daarbij heel weinig stroom
  }
  const char compile_date[] = __DATE__ " " __TIME__;
  Serial.println(compile_date);
  DrawText(0, 20, true, compile_date);
  DrawText(0, 30, false, "Version: ");
  DrawText(50, 30, false, Version);

  //==============================================
  digitalWrite(Enable_Mp3, HIGH); // Zet MP3 player aan

  // Helmet ID ====================================
  MyId = GetHelmID(); // Haal HelmID op
  ShowHelmID();

  //====== BLE (Bluetooth Low Energy) =================
  ble.begin(GetHelmName()); // Stel advertisement naam in
                            //  ble.esp_bredr_tx_power_set(esp_power_level_t min_power_level, esp_power_level_t max_power_level);
  Serial.println("Init BLE");

  //====== LedSpot ====================================
  ledcSetup(ledChannel_0, freq, resolution);
  ledcAttachPin(LedSpot, ledChannel_0);
  LedSpotLevel = GetLedSpotLevel();
  SetLedSpotLevel(LedSpotLevel);
  LedSpotTarget = LedSpotLevel;
  Serial.println("Init LedSpot");

//====== MP3 ========================================
  delay(500);
  digitalWrite(Enable_Mp3, HIGH);
  delay(500);
  if (!myDFPlayer.begin(Mp3Serial, false, false))
  {
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
  }

  AudioVolume = GetAudioVolume(); // Read MP3 audiovolume 
  //AudioVolume = 20; // lees potmeterwaarde

  myDFPlayer.volume(AudioVolume); //Set volume value. From 0 to 30
  Serial.println("Init MP3");

  // ======= Accelerometer  =================================
  MMA8452.InitMotion(MMA8452Sensib, false); // Initialiseerbeweging parameters in MM8452 accelerometer chip
  Serial.println("Init MMA8452-motion");
 // MMA8452.InitOriDet(5, true); // Initialiseer orientatie detectie in MM8452 accelerometer chip
  Serial.println("Init MMA8452-Orientatie");
  //  MMA8452.InitSleep(255,true);  // Initialiseer Sleep en wakeup functie in MM8452 accelerometer chip
  Serial.println("Init MMA8452Sleep");
  attachInterrupt(digitalPinToInterrupt(Motion_intr), Motion_ISR, FALLING);
  attachInterrupt(digitalPinToInterrupt(MMA8452Int2), MMA8452Int2_ISR, FALLING);

  // ======= ESP WROOM32  =================================
  // Enable deep sleep functionaliteit
  esp_sleep_enable_timer_wakeup(1000000LL * 1800); // Set timer to wake up the proccesor
  esp_sleep_enable_ext0_wakeup(MMA8452Int2, 0); //1 = High, 0 = Low SleepWake_intr

  Serial.println("Enable ESP-Wroom32 Sleep function");
   esp_efuse_read_mac(chipid);
   Serial.print("ESP32 MAC = ");
   Serial.printf("%02x:%02x:%02x:%02x:%02x:%02x\n",chipid[0], chipid[1], chipid[2], chipid[3], chipid[4], chipid[5]);
   //Serial.printf("%X\n",chipid);

  // ======== Initialize Timer0 interrupt =========================
  // define frequency of interrupt
  timer0 = timerBegin(0, 80, true);
  // define the interrupt callback function
  timerAttachInterrupt(timer0, &onTimer0, true);
  // start the timer
  // Set alarm to call onTimer function every 1/10 second (value in microseconds).
  // Repeat the alarm (third parameter)
  timerAlarmWrite(timer0, 100000, true);
  timerAlarmEnable(timer0);

  // Initialize timer 1 for IR sensor measurement
  pinMode(IRSignal, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(IRSignal), IR_ISR, FALLING);
  timer1 = timerBegin(1, 80, true);

 SW_Timer[SystemProcesTimer].Set(5);
 SW_Timer[OledMesgTimer].Set(5);
 SW_Timer[SendStatusTimer].Set(5);
 SW_Timer[CheckExtPowerTimer].Set(5);
 SW_Timer[BLETimeoutTimer].Set(5);
 SW_Timer[BattLowLedTimer].Set(5);
 SW_Timer[IRInhibTimer].Set(5);
 SW_Timer[CheckOrientatieTimer].Set(5);
// SW_Timer[AudioDelayTimer].Set(5);




  switch (wakeup_reason)
  {
  case ESP_SLEEP_WAKEUP_EXT0:
    MMA8452.InitSleep(255, true);        // Initialiseer Sleep en wakeup functie in MM8452 accelerometer chip; Lange inSlaaptijd
    SW_Timer[SendStatusTimer].Set(1800); // Helm gestart door bewegingssensor: Statusmeldingen na 180 seconden
    StartTune = 15;                      // Welkom bij de Schattenjacht
    break;
  case ESP_SLEEP_WAKEUP_TIMER:
    MMA8452.InitSleep(50, true);       // Initialiseer Sleep en wakeup functie in MM8452 accelerometer chip; 30 sec inSlaaptijd
    SW_Timer[SendStatusTimer].Set(50); // Helm gestart door timerinterrupt: Statusmeldingen na 5 seconden
    StartTune = 19;                    // Winfried fluitje
    break;
  default:
    MMA8452.InitSleep(50, true);       // Initialiseer Sleep en wakeup functie in MM8452 accelerometer chip; 30 sec inSlaaptijd
    SW_Timer[SendStatusTimer].Set(50); // Helm gestart door onbekende oorzaak: Statusmeldingen na 180 seconden
    break;
  }

  delay(250);
  myDFPlayer.play(StartTune);
  Serial.print("Version: ");
  Serial.println(Version);
  Serial.print("Build: ");
  Serial.println(compile_date);
 }

// ==================================================================================================
//      E N D  O F  S E T U P
// ==================================================================================================

// =================================================================================================
// Timer 0   Main system timer
// =================================================================================================

void IRAM_ATTR onTimer0()
{

  int i;

  portENTER_CRITICAL_ISR(&timerMux);

  // -------------- IR decoding:
  if (IRTimeOut > 0)
  {
    IRTimeOut--;
    if (IRTimeOut == 0) // als maximale tijdsduur van een IR code is verstreken ga dan naar "sFINISHING"
    {
      IRState = sFINISHING;
      IRFlag = 1; // Nieuwe IR Reading klaar
    }
  }

  //--------------- Software Timers:
  for (i = 0; i < NrTimers; i++)
  {
    SW_Timer[i].Update();
  }

  portEXIT_CRITICAL_ISR(&timerMux);
}
// ============= End of Timer0_IRQ ==============================


// =================================================================================================
// Scheduler
// =================================================================================================

void DoScheduler(void)
{
int MyInt;
byte MyOri = 0; // Variable tbv bepalen van helm orientatie. Nodig voor versneld uitschakelen als helm aan haak hangt

  CheckIRCode();     // Kijk of er een infrarood code ontvangen is
  CheckMotion();     // Handel accelerometer interrupts af
  CheckSleepModus(); // Kijk of helm in slaapstand kan worden gezet

  if (SW_Timer[SystemProcesTimer].Expired())
  {
    SW_Timer[SystemProcesTimer].Set(1); // Om de 1/10 seconde

    //=== Check ledspot level ====
    LedSpotTarget = GetLedSpotLevel();
    if (abs(LedSpotLevel - LedSpotTarget) > 1)
    {
      LedSpotLevel = LedSpotTarget;
      SetLedSpotLevel(LedSpotLevel);
      ShowMesgP("Spotl", LedSpotLevel);
    }
    //=== Check HelmID ====
    MyInt = GetHelmID();
    if (MyInt != MyId)
    {
      MyId = MyInt;
      ShowHelmID();
      Serial.print("ID = ");
      Serial.println(MyId);
    }
    //=== Check Audiovolume level ====
    MyInt = GetAudioVolume();
    if (abs(MyInt - AudioVolume) > 1)
    {
      Serial.print("AudioVolume: ");
      Serial.println(MyInt);
      AudioVolume = MyInt;
      ShowMesgP("Volume", AudioVolume);
      myDFPlayer.volume(AudioVolume); //stel volume in. tussen 0 to 30
    }
  }

  if (SW_Timer[OledMesgTimer].Expired())
  {
    SW_Timer[OledMesgTimer].Halt();
    ShowMesg(""); // Wis laatste boodschap
  }
// Als SendStatusTimer Expired is dan worden een aantal status parameters verzonden
  if (SW_Timer[SendStatusTimer].Expired())
  {
    switch (StatItems)
    {
    case sIDLESTAT:
      Serial.println("Status msg = idle");
      ble.begin(GetHelmName());
      SW_Timer[SendStatusTimer].Set(9000); // 15 minuten
      StatItems = sVBAT;
      break;
    case sVBAT:
      Serial.println("Status msg = VBAT");
      AdvertiseVBattMsg();
      SW_Timer[SendStatusTimer].Set(50); // 5 seconden
      StatItems = sHELMTEMP;
      break;

    case sHELMTEMP:
      Serial.println("Status msg = HELMTEMP");
      AdvertiseTempMsg();
      SW_Timer[SendStatusTimer].Set(50); // 5 seconden
      StatItems = sHELMGauge;
      break;

    case sHELMGauge:
      Serial.println("Status msg = HELMGAUGE");
      AdvertiseGaugeMsg();
      SW_Timer[SendStatusTimer].Set(50); // 5 seconden
      StatItems = sHELMBatP;
      break;

    case sHELMBatP:
      Serial.println("Status msg = HELMBAT%");
      AdvertiseBatPMsg();
      SW_Timer[SendStatusTimer].Set(50); // 5 seconden
      StatItems = sIDLESTAT;
      break;
    }
  }

  if (SW_Timer[BLETimeoutTimer].Expired())
  {
    SW_Timer[BLETimeoutTimer].Halt(); // Timer in Halt modus;
    Serial.println(GetHelmName());
    ble.begin(GetHelmName()); // BLE advertising name
  }

  if (SW_Timer[BattLowLedTimer].Expired())
  {
    if (BLL)
    {
      SW_Timer[BattLowLedTimer].Set(30); // Om de drie seconden
      BLL = false;
      digitalWrite(BattLowLed, LOW);
    }
    else
    {
      SW_Timer[BattLowLedTimer].Set(1); // 100 Ms
      BLL = true;
      if (BattGauge.GetPerc() < 34) // Als de accu onde de 20% komt gaat led knipperen
        digitalWrite(BattLowLed, HIGH);
    }
  }

  if (SW_Timer[CheckExtPowerTimer].Expired())
  {
    ShowEnergyStatus();
    SW_Timer[CheckExtPowerTimer].Set(20); // Om de twee seconden
    if (IsExternalVoltage()== true)   // External power detected
    {
      if (OprMode != Charging)
      {
        Serial.println("Charging");
        OprMode = Charging;
 
        display.clearDisplay();        // Clear Oled
        digitalWrite(Enable_Mp3, LOW); // Switch off (power consuming) MP3Player
        digitalWrite(BattLowLed, LOW); // Switch off BattLowLed

        //SetLedSpotLevel(0);
      }
      else
      {
         ShowEnergyStatus();
        ShowHelmID();
      }
    }
  }
   if (SW_Timer[CheckOrientatieTimer].Expired())
  {
    SW_Timer[CheckOrientatieTimer].Set(10); // Elke seconde.
    MyOri = MMA8452.readRegister(OUT_Y_MSB);
    if(MyOri > 127)
      MyOri = 255 - MyOri;
    if(MyOri > 56) // helm hangt aan haak
      HelmOriCnt++;
    else
      HelmOriCnt = 0;
    if((wakeup_reason != ESP_SLEEP_WAKEUP_TIMER) && (HelmOriCnt == 5))
    {
       MMA8452.InitMotion(4, false); // Initialiseer
       MMA8452.InitSleep(3,true); // Indien er geen beweging wordt gemeten gaat helm binnen een paar sec in slaapstand
       Serial.println("De helm hangt aan een haak. Snel weer slapen");
    }
       // 
  }
  if(SW_Timer[AudioDelayTimer].Expired())
  {
    SW_Timer[AudioDelayTimer].Halt();
    PlaySound(MyTrack);
  }  
}

// ============= Einde Scheduler ==========================


// =================================================================================================
// Infraroodcode interrupt routine
// =================================================================================================
void IRAM_ATTR IR_ISR()
{
  portENTER_CRITICAL_ISR(&mux);
  NrIR_ISR++;
  switch (IRState)
  {
  case sWAITING:
    IRState = sPENDING; // Verander IRState zodat bij de volgende afhandeling het sPENDING gedeelte wordt doorlopen
    IRTr_p = 0;         // zet index van IRTrancient array op nul
    IRTimeOut = 3;      // IRTimeOut wordt door Timer0 elke 100 Ms ge-decremeerd. Timeout dus na 300 ms na eerste trancient
    IRFlag = false;     // maak IRFlag 'false' (Ten overvloede)
    break;

  case sPENDING:
    IRTrancient[IRTr_p].num64 = timerRead(timer1); // sla tijdstip van neergaande trancient op
    timerWrite(timer1, 0);                         // Zet timer weer terug op nul
    IRTr_p++;
    if (IRTr_p >= MaxIRTrans) // als maximale aantal IR trancients binnengekomen zijn ga dan naar "sFINISHING"
    {
      IRState = sFINISHING;
//      Serial.println("Max pulses");
      IRFlag = true; // IRFlag true wegens maximaal aantal pulsen
      break;
    }
    break;

  case sFINISHING: // Deze State wordt niet in deze ISR afgehandeld maar in Timer5 ISR

    break;
  }
  portEXIT_CRITICAL_ISR(&mux);
}
// ============= Einde infrorood ontvanger ==========================



void loop()
{
  DoScheduler();    // Voer "ge-schedulde" taken uit
}
